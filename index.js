#!/usr/bin/env node

const AWS = require("aws-sdk");
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const files = require('./lib/files');
const questions = require('./lib/questions');
const directory = require('./lib/directory');

var currentDirectory, currentDirectoryName, currentSchema, directorySchema  = "None";
var directoryData, currentSchemaData = {};
var clouddirectory = {};
var directoriesList = [];

//Main CLI run loop
const runLoop = async () => {
  let quit = false;
  // Get directories and make user chooses one
  await listDirectories();
  await startDirecctoryInfo();
  await getSchemaInfo(currentSchema);
  while (!quit) {
    clear();
    console.log(
      chalk.yellow(
        figlet.textSync('Cloud Directory', { horizontalLayout: 'full' })
      )
    );
    clouddirectory = new AWS.CloudDirectory({apiVersion: '2017-01-11'});

    console.log(chalk.green("Current AWS Profile: "), AWS.config.credentials.profile);
    console.log(chalk.green("Current Region: "), AWS.config.region);
    console.log(chalk.green("Current Directory Name: "), currentDirectoryName);
    console.log(chalk.green("Current Directory ARN: "), currentDirectory);
    console.log(chalk.green("Current Directory Schema ARN: "), currentSchema );
    console.log(chalk.green("Current Published Schema:"), directorySchema);
    console.log("");
    const action = await questions.getAction();
    switch (action) {
      case "Get Directory Info":
        await directory.getDirectoryInfo(clouddirectory, currentDirectory);
        await getSchemaInfo(currentSchema);
        quit = await getContinue();
      break;
      case "Change Directories":
        await listDirectories();
        await directory.getDirectoryInfo(clouddirectory, currentDirectory);
        await getSchemaInfo(currentSchema);
      break;
      case "Apply Schema":
        await applySchema();
        quit = await getContinue();
      break;
      case "Get Published Schema":
        await getPublishedSchema();
        quit = await getContinue();
      break;
      case "Get Schema Info":
        await getSchemaInfo(currentSchema);
        quit = await getContinue();
      break;
      case "Create Object":
        await createObject();
        quit = await getContinue();
      break;
      case "Connect Object":
        await connectObject();
        quit = await getContinue();
      break;
      case "Get Root Object":
        await getRoot();
        quit = await getContinue();
      break;
      case "Get Object Info":
        await displayObjectInfo();
        quit = await getContinue();
      break;
      case "Quit":
        quit = true;
      break;
      default:
    }
  }

  console.log("Exiting...");
}

const getContinue = async () => {
  const resp = await questions.startOver();
  if (resp === "No") {
    return true
  } else {
    return false;
  }
}

const getCreds = async () => {
  if (!getAWSConfig())
  await cd.getRegionInfo();
}

const getAWSConfig = () => {
  AWS.config.getCredentials( err => {
    if (err)  {
      console.log(err.stack)
      console.error("No AWS credentials setup. closing...");
      process.exit(0);
    } else {
      console.log(chalk.green("Current AWS Profile:", AWS.config.credentials.profile));
      return AWS.config.region
    }
  });
}

const getRegionInfo = async () => {
  if (AWS.config.region) {
    console.log(chalk.green("Current Region: ", AWS.config.region));
    clouddirectory = new AWS.CloudDirectory({apiVersion: '2017-01-11'});
  } else {
    console.log(chalk.red("No AWS region set."))
    const response = await questions.getAWSRegion();
    console.log(chalk.green("New Region: ", response));
    AWS.config.update({region: response});
    clouddirectory = new AWS.CloudDirectory({apiVersion: '2017-01-11'});
  }
}

//Create Directory
const createDirectory = async () => {
  // Get Name
  var params = {
    Name: 'STRING_VALUE', /* required */
    SchemaArn: currentSchema /* required */
  };
  clouddirectory.createDirectory(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });
}

//Get list of all directories
const listDirectories = async () => {
  let params = {
    MaxResults: '10',
    state: "ENABLED"
  };

  await new Promise(resolve => {
    return clouddirectory.listDirectories(params, async (err, data) => {
      if (err) console.log(err, err.stack); // an error occurred
      else     {
        data.Directories.forEach( directory => {
          directoriesList.push({
            "name": directory.Name,
            "arn": directory.DirectoryArn
          })
        });

        if (directoriesList.length === 0) {
          //TODO Create Directory or Quit
          return
        }

        // Ask selection question with directoriesList
        const selectedDir = await questions.selectDirectory(directoriesList);
        // Update Selected Directory
        data.Directories.forEach( directory => {
          if (directory.Name === selectedDir) {
            currentDirectoryName = directory.Name;
            currentDirectory = directory.DirectoryArn;
          }
        });

        directoriesList = [];
      }

      return resolve();
    });
  });
}


//Get Current Directory info
const startDirecctoryInfo = async () => {
  let params = {
    DirectoryArn: currentDirectory /* required */
  };

  await new Promise(resolve => {
    return clouddirectory.getDirectory(params, (err, data) => {
      if (err) console.log(err, err.stack); // an error occurred
      else     {
        console.log(data);           // successful response
        if (data['Directory']["Name"]) {
          currentDirectoryName = data['Directory']["Name"];
          currentDirectory = data["Directory"]["DirectoryArn"];
        }
      }

      return resolve()
    });
  });

  params = {
    DirectoryArn: currentDirectory, /* required */
    // MaxResults: 'NUMBER_VALUE',
    // NextToken: 'STRING_VALUE',
    // SchemaArn: 'STRING_VALUE'
  };

  await new Promise(resolve => {
    return clouddirectory.listAppliedSchemaArns(params, async function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     currentSchema = await questions.selectSchema(data.SchemaArns);

      return resolve();
    });
  });
}

//Apply Current Schema
const applySchema = async () => {
  let params = {
    DirectoryArn: currentDirectory, /* required */
    PublishedSchemaArn: currentSchema /* required */
  };
  await new Promise(resolve => {
    clouddirectory.applySchema(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
      //Set applied schema arn from this data

      return resolve();
    });
  })
}

//List all schema in published state
const getPublishedSchema = async () => {
  let schemaArr = [];
  let params = {
    // MaxResults: 'NUMBER_VALUE',
    // NextToken: 'STRING_VALUE',
    // SchemaArn: 'STRING_VALUE'
  };
  await new Promise(resolve => {
    clouddirectory.listPublishedSchemaArns(params, async function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     currentSchema = await questions.selectSchema(data.SchemaArns);

      return resolve();
    });
  });
}

//Get Schema Info
const getSchemaInfo = async (schemaArn) => {
  var params = {
    SchemaArn: schemaArn /* required */
  };

  await new Promise(resolve => {
    clouddirectory.getSchemaAsJson(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else  {
        let schemaJson = JSON.parse(data.Document);
        console.log(schemaJson);
        directorySchema = schemaJson.sourceSchemaArn;
      }
      return resolve();
    });
  })
}


//Create Object
const createObject = async () => {
  let facetName = await questions.getFacetName();
  let parentLink = await questions.getParentLink();
  let linkName = await questions.getLinkName();
  let attrArr = [];
  let stop = true;

  while (stop) {
    const key = await questions.getInput("Enter the Facet Key Name:");
    const value = await questions.getInput("Enter the key value(only String types):");
    attrArr.push({
      Key: {
        FacetName: facetName,
        Name: key,
        SchemaArn: currentSchema
      },
      Value: {
        StringValue: value
      }
    });

    let resp = await questions.anotherattribute();
    if (resp === "No") stop = false;
  }

  let params = {
    DirectoryArn: currentDirectory,
    SchemaFacets: [
      {
        FacetName: facetName,
        SchemaArn: currentSchema
      }
    ],
    LinkName: linkName,
    ObjectAttributeList: attrArr,
    ParentReference: {
      Selector: parentLink
    }
  };

  console.log(params);

  await new Promise(resolve => {
    clouddirectory.createObject(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else  {
        console.log(data);
      }
      return resolve();
    });
  })
}



//Get Root Object Children
const getRoot = async () => {
  let params = {
    DirectoryArn: currentDirectory, /* required */
    ObjectReference: { /* required */
      Selector: '/'
    },
    ConsistencyLevel: 'EVENTUAL'
  };

  await new Promise(resolve => {
    clouddirectory.listObjectChildren(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
      return resolve();
    });
  })
}

//List Root indecies
const listIndices = async () => {
  var params = {
    DirectoryArn: currentDirectory, /* required */
    TargetReference: { /* required */
      Selector: '/'
    },
    ConsistencyLevel: 'EVENTUAL'
  };

  await new Promise(resolve => {
    clouddirectory.listAttachedIndices(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
      return resolve();
    });
  })
}

//List connect object
const connectObject = async () => {
  const object = await questions.getInput('Enter the Object Selector:');
  const linkName = await questions.getLinkName();
  const parentSelector = await questions.getParentLink();
  var params = {
    ChildReference: { /* required */
      Selector: object
    },
    DirectoryArn: currentDirectory, /* required */
    LinkName: linkName, /* required */
    ParentReference: { /* required */
      Selector: parentSelector
    }
  };

  await new Promise(resolve => {
    clouddirectory.attachObject(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else     console.log(data);           // successful response
      return resolve();
    });
  });
}


const displayObjectInfo = async () => {
  let selector2 = await questions.getLinkName();
  // let facetName = await questions.getFacetName();
  let operationsArr = [];
  // let objectdata = {`${selector2}`: {}};

  operationsArr.push({
      ListObjectParentPaths: {
        ObjectReference: { /* required */
          Selector: selector2
        }
      }
  });

  operationsArr.push({
      ListObjectAttributes: {
        ObjectReference: { /* required */
          Selector: selector2
        }
      },
  });

  var params = {
    DirectoryArn: currentDirectory, /* required */
    Operations: operationsArr,
    ConsistencyLevel: 'EVENTUAL'
  };

  await new Promise(resolve => {
    clouddirectory.batchRead(params, function(err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      // else     console.log(data);           // successful response
      data.Responses.forEach(response => {
        if (response.SuccessfulResponse) {
          if (response.SuccessfulResponse.ListObjectParentPaths) {
            let pathsResp = response.SuccessfulResponse.ListObjectParentPaths.PathToObjectIdentifiersList;
            pathsResp.forEach(pathObject => {
              console.log(pathObject);
              // pathSplit = pathObject.split('/');

            });
          } else if (response.SuccessfulResponse.ListObjectAttributes) {
            console.log('Attributes');
            let attrs = response.SuccessfulResponse.ListObjectAttributes.Attributes;
            attrs.forEach(attr => {
              const value = Object.values(attr.Value).forEach(val => {
                if (val) console.log(attr.Key.Name, ':', val);
              });
            });
          }
        }
        else console.log(response);
        console.log('');
      })
      return resolve();
    });
  });
}

//Program start - Get credentials, start Loop
clear();
console.log(
  chalk.yellow(
    figlet.textSync('Cloud Directory', { horizontalLayout: 'full' })
  )
);
if (!getAWSConfig()) {
  getRegionInfo().then(() => {
    runLoop();
  })
} else {
  runLoop();
}
