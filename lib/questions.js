const inquirer = require('inquirer');

module.exports = {
  getAction: async () => {
    const questionTemplate = [
      {
        name: 'action',
        type: 'list',
        message: 'What would you like to do?',
        choices: [
          "Get Directory Info",
          // "Get Published Schema",
          // "Get Schema Info",
          "Get Root Object",
          "Get Object Info",
          "Create Object",
          "Connect Object",
          // "Apply Schema",
          // "Change Directories",
          "Quit",
        ]
      }
    ];
    let response = await inquirer.prompt(questionTemplate);
    return response.action;
  },
  startOver: async () => {
    const questionTemplate = [
      {
        name: 'action',
        type: 'list',
        message: 'Continue?',
        choices: [
          "Yes",
          "No",
        ]
      }
    ];
    let response = await inquirer.prompt(questionTemplate);
    return response.action;
  },
  anotherattribute: async () => {
    const questionTemplate = [
      {
        name: 'action',
        type: 'list',
        message: 'Add Another Attribute',
        choices: [
          "Yes",
          "No",
        ]
      }
    ];
    let response = await inquirer.prompt(questionTemplate);
    return response.action;
  },
  getAWSRegion: async () => {
    const questionTemplate = [
      {
        name: 'region',
        type: 'list',
        message: 'Select the region to use:',
        choices: [
          "us-east-1",
          "us-east-2",
          "us-west-2",
          "ca-central-1",
          "ap-southeast-1",
          "ap-southeast-2",
          "eu-west-1",
          "eu-west-2",
          "eu-central-1"
        ]
      }
    ];
    let response = await inquirer.prompt(questionTemplate);
    return response.region;
  },
  selectDirectory: async (directoryList) => {
    const dirNames = directoryList.map(directory => directory.name);
    const questionTemplate = [
      {
        name: 'directory',
        type: 'list',
        message: 'Select the directory to use:',
        choices: dirNames
      }
    ];
    let response = await inquirer.prompt(questionTemplate);
    return response.directory;
  },
  selectSchema: async (schemaArr) => {
    const questionTemplate = [
      {
        name: 'schema',
        type: 'list',
        message: 'Select the schema to use:',
        choices: schemaArr
      }
    ];
    let response = await inquirer.prompt(questionTemplate);
    return response.schema;
  },
  getLinkName: async () => {
    const questionTemplate = {
      name: 'answer',
      type: 'input',
      message: 'Enter the Link Name for the object:'
    };
    let response = await inquirer.prompt(questionTemplate);
    return response.answer;
  },
  getFacetName: async () => {
    const questionTemplate = {
      name: 'answer',
      type: 'input',
      message: 'Enter the Facet Name for the object:'
    };
    let response = await inquirer.prompt(questionTemplate);
    return response.answer;
  },
  getParentLink: async () => {
    const questionTemplate = {
      name: 'answer',
      type: 'input',
      message: 'Enter the Parent Selector for the object:'
    };
    let response = await inquirer.prompt(questionTemplate);
    return response.answer;
  },
  getName: async () => {
    const questionTemplate = {
      name: 'answer',
      type: 'input',
      message: 'Enter the Name for the object:'
    };
    let response = await inquirer.prompt(questionTemplate);
    return response.answer;
  },
  getUserID: async () => {
    const questionTemplate = {
      name: 'answer',
      type: 'input',
      message: 'Enter the user ID for the object:'
    };
    let response = await inquirer.prompt(questionTemplate);
    return response.answer;
  },
  getInput: async (message) => {
    const questionTemplate = {
      name: 'answer',
      type: 'input',
      message: `${message}:`
    };
    let response = await inquirer.prompt(questionTemplate);
    return response.answer;
  }
};
