module.exports = {
  //Get Current Directory info
  getDirectoryInfo: async (clouddirectory, currentDirectory) => {
    let params = {
      DirectoryArn: currentDirectory /* required */
    };

    await new Promise(resolve => {
      return clouddirectory.getDirectory(params, (err, data) => {
        if (err) console.log(err, err.stack); // an error occurred
        else     {
          console.log(data);           // successful response
        }

        return resolve();
      });
    });

    params = {
      DirectoryArn: currentDirectory,
    };

    await new Promise(resolve => {
      return clouddirectory.listAppliedSchemaArns(params, async function(err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else     console.log(data);

        return resolve();
      });
    });
  },
}
