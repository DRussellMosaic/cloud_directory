
// //List directories
var params = {
  MaxResults: '10',
  state: "ENABLED"
};
clouddirectory.listDirectories(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log('List Directories', data);           // successful response
});

// list schemas
var params = {
  MaxResults: '10'
};
clouddirectory.listDevelopmentSchemaArns(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});

var params = {
  DirectoryArn: DIRECTORYARN,
};
clouddirectory.listAppliedSchemaArns(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log('Applied Schemas', data);           // successful response
});

//Get Schema info
var params = {
  SchemaArn: SCHEMAARN /* required */
};
clouddirectory.getSchemaAsJson(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(JSON.stringify(data, null, 2));           // successful response
});

//Apply Schema
// var params = {
//   DirectoryArn: DIRECTORYARN, /* required */
//   PublishedSchemaArn: SCHEMAARN /* required */
// };
// clouddirectory.applySchema(params, function(err, data) {
//   if (err) console.log(err, err.stack); // an error occurred
//   else     console.log("Applying a schema:", data);           // successful response
// });

//Publish Schema
// var params = {
//   DevelopmentSchemaArn: SCHEMAARN, /* required */
//   Version: '1.0', /* required */
// };
// clouddirectory.publishSchema(params, function(err, data) {
//   console.log('Publish Schema:');
//   if (err) console.log(err, err.stack); // an error occurred
//   else     console.log(data);           // successful response
// });
//
//
// //Create object in directory
// var params = {
//   DirectoryArn: DIRECTORYARN, /* required */
//   SchemaFacets: [ /* required */
//     {
//       FacetName: 'Organization',
//       SchemaArn: SCHEMAARN
//     }
//   ],
//   LinkName: 'Main',
//   ObjectAttributeList: [
//     {
//       Key: { /* required */
//         FacetName: 'Organization', /* required */
//         Name: 'email', /* required */
//         SchemaArn: SCHEMAARN /* required */
//       },
//       Value: { /* required */
//         // BinaryValue: Buffer.from('...') || 'STRING_VALUE' /* Strings will be Base-64 encoded on your behalf */,
//         // BooleanValue: true || false,
//         // DatetimeValue: new Date || 'Wed Dec 31 1969 16:00:00 GMT-0800 (PST)' || 123456789,
//         // NumberValue: 'STRING_VALUE',
//         StringValue: 'druss@test.com'
//       }
//     }
//   ]
// };
// clouddirectory.createObject(params, function(err, data) {
//   if (err) console.log(err, err.stack); // an error occurred
//   else     console.log(data);           // successful response
// });


//Java Example from aws docs
/*
//Read the JSON schema content from the file.
 String jsonFilePath = <Provide the location of the json schema file here>;
 String schemaDocument;
 try
 {
     schemaDocument = new String(Files.readAllBytes(Paths.get(jsonFilePath)));
 }
 catch(IOException e)
 {
     throw new RuntimeException(e);

 //Create an empty schema with a schema name. The schema name needs to be unique
 //within an AWS account.
 CreateSchemaRequest createSchemaRequest = new CreateSchemaRequest()
     .withName("EmployeeSchema");
 String developmentSchemaArn =  client.createSchema(createSchemaRequest).getSchemaArn();

 //Load the previously defined JSON into the empty schema that was just created
 PutSchemaFromJsonRequest putSchemaRequest = new PutSchemaFromJsonRequest()
        .withDocument(schemaDocument)
        .withSchemaArn(developmentSchemaArn);
 PutSchemaFromJsonResult putSchemaResult =  client.putSchemaFromJson(putSchemaRequest);

 //No more changes needed for schema so publish the schema
 PublishSchemaRequest publishSchemaRequest = new PublishSchemaRequest()
     .withDevelopmentSchemaArn(developmentSchemaArn)
     .withVersion("1.0");
 String publishedSchemaArn =  client.publishSchema(publishSchemaRequest).getPublishedSchemaArn();



 //Create a directory using the published schema. Specify a directory name, which must be unique within an account.
 CreateDirectoryRequest createDirectoryRequest = new CreateDirectoryRequest()
     .withName("EmployeeDirectory")
     .withSchemaArn(publishedSchemaArn);
 CreateDirectoryResult createDirectoryResult =  client.createDirectory(createDirectoryRequest);
 String directoryArn = createDirectoryResult.getDirectoryArn();
 String appliedSchemaArn = createDirectoryResult.getAppliedSchemaArn();



 for (String groupName : Arrays.asList("ITStaff", "Managers")) {
 CreateObjectRequest request = new CreateObjectRequest()
     .withDirectoryArn(directoryArn)
    // The parent of the object we are creating. We are rooting the group nodes
    // under root object. The root object exists in all directories and the path
    // to the root node is always "/".
    .withParentReference(new ObjectReference().withSelector("/"))
    // The name attached to the link between the parent and the child objects.
    .withLinkName(groupName)
    .withSchemaFacets(new SchemaFacet()
        .withSchemaArn(appliedSchemaArn)
        .withFacetName("Group"))
        //We specify the attributes to attach to this object.
         .withObjectAttributeList(new AttributeKeyAndValue()
             .withKey(new AttributeKey()
                      // Name attribute for the group
                      .withSchemaArn(appliedSchemaArn)
                      .withFacetName("Group")
                      .withName("Name"))
                 // We provide the attribute value. The type used here must match the type defined in schema
                      .withValue(new TypedAttributeValue().withStringValue(groupName)));
client.createObject(request);
}



CreateObjectRequest createAnna = new CreateObjectRequest()
           .withDirectoryArn(directoryArn)
           .withLinkName("Anna")
           .withParentReference(new ObjectReference().withSelector("/ITStaff"))
           .withSchemaFacets(new SchemaFacet()
                   .withSchemaArn(appliedSchemaArn)
                   .withFacetName("Employee"))
           .withObjectAttributeList(new AttributeKeyAndValue()
                   .withKey(new AttributeKey()
                           // Name attribute from employee facet
                           .withSchemaArn(appliedSchemaArn)
                           .withFacetName("Employee")
                           .withName("Name"))
                   .withValue(new TypedAttributeValue().withStringValue("Anna")),
                   new AttributeKeyAndValue()
                   .withKey(new AttributeKey()
                           // EmailAddress attribute from employee facet
                           .withSchemaArn(appliedSchemaArn)
                           .withFacetName("Employee")
                           .withName("EmailAddress"))
                   .withValue(new TypedAttributeValue().withStringValue("anna@somecorp.com")),
                   new AttributeKeyAndValue()
                   .withKey(new AttributeKey()
                            // Status attribute from employee facet
                           .withSchemaArn(appliedSchemaArn)
                           .withFacetName("Employee")
                           .withName("Status"))
                   .withValue(new TypedAttributeValue().withStringValue("ACTIVE")));
// CreateObject provides the object identifier of the object that was created.  An object identifier
  // is a globally unique, immutable identifier assigned to every object.
String annasObjectId = client.createObject(createAnna).getObjectIdentifier();


AttachObjectRequest makeAnnaAManager = new AttachObjectRequest()
         .withDirectoryArn(directoryArn)
         .withLinkName("Anna")
         // Provide the parent object that Anna needs to be attached to using the path to the Managers object
         .withParentReference(new ObjectReference().withSelector("/Managers"))
         // Here we use the object identifier syntax to specify Anna's node. We could have used the
         // following path instead: /ITStaff/Anna. Both are equivalent.
         .withChildReference(new ObjectReference().withSelector("$" + annasObjectId));
 client.attachObject(makeAnnaAManager);


 // First get the object for Anna
  GetObjectInformationRequest annaObjectRequest = new GetObjectInformationRequest()
         .withObjectReference(new ObjectReference().withSelector("/Managers/Anna"))
         .withDirectoryArn(directoryArn);
  GetObjectInformationResult annaObjectResult =  client.getObjectInformation(annaObjectRequest);
  // List parent objects for Anna to give her groups
  ListObjectParentsRequest annaGroupsRequest = new ListObjectParentsRequest()
         .withDirectoryArn(directoryArn)
         .withObjectReference(new ObjectReference().withSelector("$" + annaObjectResult.getObjectIdentifier()));
  ListObjectParentsResult annaGroupsResult =  client.listObjectParents(annaGroupsRequest);
  for(Map.Entry<String, String> entry : annaGroupsResult.getParents().entrySet())
  {
     System.out.println("Parent Object Identifier:" + entry.getKey());
     System.out.println("Link Name:" + entry.getValue());
  }


}*/
